<?php

require_once __DIR__ . '/vendor/autoload.php';

use Indexer\Dictionary;
use Indexer\TextFile;



$options = getopt('f:');

$fileName = isset($options['f']) ? $options['f'] : null;

if (!file_exists($fileName)) {
    die('File not found');
}

$size = filesize($fileName);
$dictionary = new Dictionary(__DIR__ . '/data/index');
$textReader = new TextFile($fileName);

$lastWord = '';
$curr = 0;
while ($text = $textReader->read()) {
    $curr += strlen($text);
    $perc = round($curr / $size * 100, 2);

    if ($perc % 5 == 0) {
        print $perc . '%' . PHP_EOL;
    }
    $text = preg_replace('/[^\w]/u', ' ', $text);
    $text = preg_replace('/[ ]+/', ' ', $text);
    $words = explode(' ', $lastWord . trim($text));
    $lastWord = array_shift($words);
    foreach ($words as $word) {
        $dictionary->addOrIncrement(mb_strtolower($word));
    }
}
$dictionary->addOrIncrement(mb_strtolower($lastWord));

print  memory_get_peak_usage() . PHP_EOL;