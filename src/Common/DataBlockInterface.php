<?php

namespace Common;

interface DataBlockInterface
{
    public function __toString();
    public function parse($block);
    public function getPosition();
}