<?php
namespace Common;

interface DataIOInterface
{
    /**
     * @return DataBlockInterface
     */
    public function read();
    public function write(DataBlockInterface $data);
}