<?php


namespace Indexer;


use Common\DataBlockInterface;

class Cache
{
    protected $cacheSize;
    protected $callback;
    protected $items;

    public function __construct($callback = null, $cacheSize = 1000)
    {
        $this->callback = $callback;
        $this->cacheSize = $cacheSize;
    }

    public function dropCache()
    {
        foreach ($this->items as $item) {
            $this->callback($item);
        }
    }

    public function set($key, DataBlockInterface $data)
    {
        if (!isset($this->items[$key]) &&
            (count($this->items) + 1 > $this->cacheSize)
        ) {
            $item = array_shift($this->items);
            $this->callback($item);
        }
        if (!isset($this->items[$key])) {
            $this->items[$key] = $data;
        }
    }

    public function callback($item)
    {
        if (is_callable($this->callback)) {
            $callback = $this->callback;
            $callback($item);
        }
    }

    public function get($key)
    {
        if (isset($this->items[$key])) {
            return $this->items[$key];
        }
        return false;
    }
}