<?php

namespace Indexer;
interface IndexerInterface
{
    /**
     * @param string $word
     * @return mixed
     */
    public function addWord($word);

    /**
     * @param string $word
     * @return mixed
     */
    public function searchWord($word);
}