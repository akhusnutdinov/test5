<?php
namespace Indexer;

use Common\DataBlockInterface;

/**
 * Class DictionaryItem
 * @package Indexer
 */
class DictionaryItem implements DataBlockInterface
{
    protected $name;
    protected $count;
    protected $position = null;

    public function __construct($name = '', $count = 1)
    {
        $this->name = $name;
        $this->count = 1;
    }

    public function __toString()
    {
        $data = [
            str_pad(dechex(strlen($this->name)), 4, ' '),
            $this->name,
            str_pad(dechex($this->count), 4, ' '),
        ];
        return implode('', $data);
    }

    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getPosition() {
        return $this->position;
    }

    public function parse($block)
    {
        $this->name = substr($block, 0, strlen($block) - 4);
        $this->count = hexdec(substr($block, strlen($this->name), 4));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function incrementCounter() {
        $this->count++;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }
}