<?php
namespace Indexer;
use Common\DataBlockInterface;
use Common\DataIOInterface;

class TextFile implements DataIOInterface
{
    protected $fileHandle;
    const BLOCK_SIZE = 1024;

    public function __construct($filename)
    {
        $this->fileHandle = fopen($filename, 'r');
        if (!$this->fileHandle) {
            throw new \Exception('File error');
        }
    }

    public function __destruct()
    {
        if ($this->fileHandle) {
            fclose($this->fileHandle);
        }
    }

    /**
     * @return DataBlockInterface
     */
    public function read()
    {
        return fread($this->fileHandle, self::BLOCK_SIZE);
    }

    public function write(DataBlockInterface $data)
    {
        // TODO: Implement write() method.
    }
}