<?php


namespace Indexer;


use Common\DataBlockInterface;
use Common\DataIOInterface;

class DictionaryFile implements DataIOInterface
{
    protected $fileHandle;
    protected $position = 0;
    protected $offset = 0;
    protected $buf = '';
    const BLOCK_SIZE = 1024;

    public function __construct($fileName)
    {
        if (!file_exists($fileName)) {
            $mode = 'w+';
        } else {
            $mode = 'r+';
        }
        $this->fileHandle = fopen($fileName, $mode);
    }

    public function __destruct()
    {
        fclose($this->fileHandle);
    }

    public function readBuf()
    {
        $this->position = ftell($this->fileHandle);
        $this->offset = 0;
        return fread($this->fileHandle, self::BLOCK_SIZE);
    }

    public function readNext($count)
    {
        if (strlen($this->buf) < $this->offset + $count) {
            $this->buf = substr($this->buf, $this->offset, strlen($this->buf) - $this->offset);
            $this->buf .= $this->readBuf();
        }

        $data = substr($this->buf, $this->offset, $count);
        $this->offset += $count;
        $this->position += $count;
        return $data;
    }

    /**
     * @return DictionaryItem
     */
    public function read()
    {
        if (!feof($this->fileHandle)) {
            $position = ftell($this->fileHandle);
            $sizeData = fread($this->fileHandle, 4);
            if (!$sizeData) {
                return null;
            }
            $size = hexdec($sizeData);
            $block = fread($this->fileHandle, $size + 4);

            $dataBlock = new DictionaryItem();
            $dataBlock->parse($block);
            $dataBlock->setPosition($position);
            return $dataBlock;
        }
        return null;
    }

    public function write(DataBlockInterface $data)
    {
        if ($data->getPosition() !== null) {
            fseek($this->fileHandle, $data->getPosition());
        } else {
            fseek($this->fileHandle, 0, SEEK_END);
        }
        fwrite($this->fileHandle, $data);
    }
}