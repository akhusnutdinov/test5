<?php

namespace Indexer;

class Dictionary
{
    protected $fileName;
    protected $cache;

    public function __construct($fileName)
    {
        $this->fileName = $fileName;
        $self = $this;
        $this->cache = new Cache(function (DictionaryItem $item) use ($self) {
            $indexWriter = new DictionaryFile($self->getFilename($item->getName()));
            $indexWriter->write($item);
            unset($indexWriter);
        });
    }

    public function addOrIncrement($word)
    {
        if ($item = $this->find($word)) {
            $item->incrementCounter();
        } else {
            $item = new DictionaryItem($word);
        }
        $this->cache->set($word, $item);
        unset($item);
    }

    public function find($word)
    {
        if ($item = $this->cache->get($word)) {
            return $item;
        }
        $indexReader = new DictionaryFile($this->getFilename($word));
        while ($item = $indexReader->read()) {
            if ($item->getName() == $word) {
                unset($indexReader);
                $this->cache->set($word, $item);
                return $item;
            }
        }
        return false;
    }

    public function getFilename($word)
    {
        return sprintf('%s.i%s', $this->fileName, substr(md5($word), 0, 2));
    }

    public function __destruct()
    {
        $this->cache->dropCache();
    }
}