<?php

use Indexer\DictionaryFile;

require_once __DIR__ . '/vendor/autoload.php';

$files = glob(__DIR__ . '/data/index.i*');

foreach ($files as $file) {
    $dictionaryFile = new DictionaryFile($file);
    while ($item = $dictionaryFile->read()) {
        print $item->getName() . ' = ' . $item->getCount() . PHP_EOL;
    }
}

print  memory_get_peak_usage() . PHP_EOL;
